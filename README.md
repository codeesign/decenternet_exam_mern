# DTC MERN EXAM

### Simple CRUD application, for book readers to register and add their favourite books to the database using MERN stack .

## Installation

```bash
# clone the repo
$ git clone https://codeesign@bitbucket.org/codeesign/decenternet_exam_mern.git

# go into app directory
$ cd decenternet_exam_mern

# install app dependencies
$ npm install

# install app client dependencies
$ cd client 
$ npm install 

# run the Express Server  
$ cd decenternet_exam_mern
$ node server.js
// running on port 5000
```

## Client NextJs App


```bash
# initialize the project

$ cd client
$ npm run dev

// on port 3000
